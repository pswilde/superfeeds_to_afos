# Package

version       = "0.1.0"
author        = "Paul Wilde"
description   = "A new awesome nimble package"
license       = "Proprietary"
srcDir        = "src"
bin           = @["SFExportsforAFOS"]


# Dependencies

requires "nim >= 2.0.0"
