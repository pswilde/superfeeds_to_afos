# Superfeeds to AFOS

**This is a fairly niche program. You'll only need it if you know what both of these applications are.**
  
This tool takes export files from Superfeeds and converts them into csv files which should be compatible with [AFOS](https://animalfeedsoftware.com) 

To run, simply build and run with a single file parameter
```sh
nimble build # build once

./SFExportsforAFOS nutrients.txt

```

It can currently handle Raw Materials, formulations and Nutrients files. Nutrients is a direct database raw data copy and not an export file. Nutrients "codes" will need to be added manually as it's my understanding Superfeeds does not have shortcodes for nutrients (only numeric IDs)  

For Formulation nutrients, the nutrients file must be parsed first and kept in memory so a lookup can happen.  
To do this, simply run with both input files as parameters, nutrients file first i.e.
```sh
./SFExportsforAFOS nutrients.txt forms_nuts.txt

```

diets are a work in progress, and may not actually be necessary.
