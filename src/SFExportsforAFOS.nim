import convert

import os


proc parseArgs(): seq[string] =
  let params = commandLineParams()
  return params


when isMainModule:
  let files = parseArgs()
  for file in files:
    runConvertOn(file)


