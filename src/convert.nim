import streams
import strutils

import converters/rawmats
import converters/nutrients
import converters/formulations
import types

proc runConvertOn*(file: string) =
  let contents = newFileStream(file, fmRead)
  var line = ""
  if not isNil(contents):
    var filetype: FileType
    while contents.readLine(line):
      if line == "": continue
      if line == "SF2000": continue
      if line.contains("MSM format"): continue
      if filetype == None:
        filetype = line.toFileType
        echo filetype
        break
    case filetype
    of RawMat: fileType.convertRawMaterials(contents)
    of Nutrients: fileType.convertNutrients(contents)
    of Formulations: fileType.convertFormulations(contents)
    of FormulationNutrients: fileType.convertFormNuts(contents)
    else: discard
