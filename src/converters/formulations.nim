import ../types
import streams
import ../exporter

import strutils
import sequtils

var formulations*: seq[Formulation]

proc getNameData(f: var Formulation, line: string) =
  var ln = line.split("#")
  ln.keepIf(proc(s: string): bool = s != "")
  f.name = ln[0]
  f.stock = ln[1]
  f.feed = ln[2]
  f.optml = ln[^1]
  f.datestamp = ln[^2]

proc convertFormulations*(filetype: FileType, contents: FileStream) =
  var line = ""
  var f = Formulation()
  var line_no = 1
  var num_rms = -1
  var curr_rms = 0
  var header_done = false
  while contents.readLine(line):
    if not header_done:
      if line == "":
        line_no += 1
        continue
      if line_no == 1:
        f.code = line
        line_no += 1
        continue
      if line_no == 2:
        f.getNameData(line)
        line_no += 1
        continue
      if line_no == 3:
        line_no += 1
        continue
      if line_no == 4:
        num_rms = line.parseInt
        curr_rms = 0
        header_done = true
        line_no += 1
        continue
    if header_done:
      let rm = line.FormulaToRawMaterial()
      f.raw_materials.add(rm)
      curr_rms += 1
      if curr_rms == num_rms:
        formulations.add(f)
        f = Formulation()
        line_no = 1
        header_done = false
  # add the last one
  formulations.add(f)
  #echo forms.filter(proc(f: Formulation): bool = f.code == "9")
  export_content(formulations)


