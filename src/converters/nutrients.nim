
import ../types
import streams
import ../exporter

import strutils
import sequtils
import tables

var nuts: seq[Nutrient]


proc convertNutrients*(filetype: FileType, contents: FileStream) =
  var line = ""
  while contents.readLine(line):
    if line == "": continue
    var nut: Nutrient
    let row = line.split("\t")
    var n = row[1].split("#")
    n.keepIf(proc(s: string): bool = s != "")
    if n.len == 0: continue
    nut.name = n[0]
    nut.code = nut.nameToCode()
    if nut.name.len == 0: continue
    if n.len > 1:
      nut.unit = n[1].toUnitCode
    if nut.unit == None:
      nut.unit = nut.name.NametoUnitCode
    nuts.add(nut)
  export_content(nuts)

proc convertFormNuts*(filetype: FileType, contents: FileStream) =
  var line = ""
  var versions: FormNutVersions
  var line_no = 1
  var curr_vers = -1
  var curr_formula = ""
  var curr_nut = Nutrient()
  while contents.readLine(line):
    if line == "": continue
    if line[0] == '^' and line_no == 1:
      let parts = line[0..^2].split(",")
      curr_vers = parts[2].parseInt
      let nut_idx = parts[3].parseInt
      curr_formula = parts[1]
      if not versions.hasKey(curr_formula):
        versions[curr_formula] = NutrientsVersion()
      let nut = nuts[nut_idx]
      curr_nut.name = nut.name
      curr_nut.code = nut.code
      # got the formulation code and nutrient name, go to the next line
      line_no = 2
      continue
    if line_no == 2:
      let parts = line.split("#")
      # 1st min, 2nd max
      curr_nut.min = parts[1]
      curr_nut.max = parts[2]
      if not versions[curr_formula].hasKey(curr_vers):
        versions[curr_formula][curr_vers] = @[]

      versions[curr_formula][curr_vers].add curr_nut
      # reset the nutrient
      curr_nut = Nutrient()
      curr_formula = ""
      # go to the next line, which should be a header
      line_no = 1
  export_content(versions)

