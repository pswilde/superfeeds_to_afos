import ../types
import streams
import strutils
import os
import parsecsv
import ../exporter

proc findRawMaterial(rms: seq[RawMaterial], code: string): RawMaterial =
  for r in rms:
    if r.code == code:
      return r

proc filterCodes(old_rms: seq[RawMaterial]): seq[RawMaterial] =
  var rms: seq[RawMaterial] = @[]
  if fileExists("./inputs/RMCodeAdjust.csv"):
    var s = newFileStream("./inputs/RMCodeAdjust.csv", fmRead)
    if s == nil:
      discard
    var x: CsvParser
    open(x,s,"./inputs/RMCodeAdjust.csv")
    var line_no = 1
    while readRow(x):
      if line_no == 1:
        line_no += 1
        continue
      if x.row[0] ==  "RM":
        var rm = old_rms.findRawMaterial(x.row[1])
        rm.code = x.row[2]
        rm.description = x.row[3]
        rm.description2 = x.row[5]
        rm.group = x.row[4]
        rms.add(rm)
    return rms
  echo "No Filtering to do"
  return old_rms

proc convertRawMaterials*(filetype: FileType, contents: FileStream) =
  var line = ""
  var rms: seq[RawMaterial]
  var rm: RawMaterial
  var header_done = false
  var line_no = 1
  while contents.readLine(line):
    if line == "": 
      line_no += 1
      continue
    if not header_done:
      if line_no == 1:
        rm.code = line
        line_no += 1
        continue
      if line_no == 2:
        rm.description = line.strip()
        line_no += 1
        continue
      if line_no == 3:
        rm.description2 = line
        line_no += 1
        continue
      if line_no == 5:
        rm.cost = line
        line_no += 1
        continue
      if line == "74": 
        # start the breakdown
        header_done = true
        continue
    if header_done:
      let m = line.RMtoNutrient()
      if m.isNutrient():
        rm.nutrients.add(m)
        continue
      else:
        rms.add(rm)
        rm = RawMaterial()
        header_done = false
        rm.code = line
        line_no = 2
  # add the last one
  rms.add(rm)
  export_content(rms)
  let filtered_rms = rms.filterCodes()
  
  export_content(filtered_rms,"output_rms_filtered.csv")

