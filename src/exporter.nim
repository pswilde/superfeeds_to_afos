import strutils
import tables

import streams
import types

const OUTPUT_DIR = "output/"
proc export_content*(rms: seq[RawMaterial], in_rms_file: string = "output_rms.csv") =
  let rms_file = OUTPUT_DIR & in_rms_file
  var rms_strm = newFileStream(rms_file, fmWrite)
  let rms_nut_file = OUTPUT_DIR & "output_rms_nutrients.csv"
  var rms_nut_strm = newFileStream(rms_nut_file, fmWrite)
  if not rms_strm.isNil and not rms_nut_strm.isNil:
    const rms_header_row = "code, name, price, round, materialGroupCode, description"
    rms_strm.writeLine(rms_header_row)
    const rms_nut_header_row = "rawMaterialCode, nutrientCode, quantity"
    rms_nut_strm.writeLine(rms_nut_header_row)
    for rm in rms:
      let row = [rm.code, rm.description, rm.cost, $rm.round, rm.group, rm.description2].join(", ")
      rms_strm.writeLine(row)
      for nut in rm.nutrients:
        if nut.name == "VOLUME": continue
        let row = [rm.code, nut.code, nut.quantity].join(", ")
        rms_nut_strm.writeLine(row)

proc export_content*(nuts: seq[Nutrient]) = 
  let nutrients_file = OUTPUT_DIR & "output_nutrients.csv"
  let nuts_strm = newFileStream(nutrients_file, fmWrite)
  if not nuts_strm.isNil:
    const header_row = "code, name, nutrientGroupCode, unitCode, abbrev, description"
    nuts_strm.writeLine(header_row)
    for nut in nuts:
      let row = [nut.code, nut.name, nut.group, $nut.unit, nut.abbrev, nut.description].join(", ")
      nuts_strm.writeLine(row)

proc export_content*(forms: seq[Formulation]) = 
  let fms_file = OUTPUT_DIR & "output_formulations.csv"
  var fms_strm = newFileStream(fms_file, fmWrite)
  let f_rms_file = OUTPUT_DIR & "output_formulations_rms.csv"
  var rms_strm = newFileStream(f_rms_file, fmWrite)
  if not fms_strm.isNil and not rms_strm.isNil:
    const fms_header = "code,name,animalGroupCode,description,sex,weightFrom,weightTo,disease,body"
    fms_strm.writeLine(fms_header)
    const rms_header = "formulaCode,rawMaterialCode,min,max"
    rms_strm.writeLine(rms_header)
    for f in forms:
      let fms_row = [f.code, f.name, f.stock, f.feed, "", "", "", "", ""].join(", ")
      fms_strm.writeLine(fms_row)
      for rm in f.raw_materials:
        let rms_row = [f.code,rm.code,"",rm.quantity].join(", ")
        rms_strm.writeLine(rms_row)

proc export_content*(versions: FormNutVersions) = 
  let fms_file = OUTPUT_DIR & "output_formulations_nutrients.csv"
  var fms_strm = newFileStream(fms_file, fmWrite)
  if not fms_strm.isNil:
    const fms_header = "formulaCode,nutrientCode,min,max,version"
    fms_strm.writeLine(fms_header)
    for (f,vs) in versions.pairs:
      if f == "": continue
      var last_ver = 1
      for (v,ns) in vs.pairs:
        if v > last_ver:
          last_ver = v
      for (v,ns) in vs.pairs:
        if v != last_ver:
          continue
        for n in ns:
          let row = [f, n.code, n.min, n.max, $v].join(", ")
          fms_strm.writeLine(row)

