import strutils
import tables
import re

type
  FileType* = enum
    None,
    RawMat,
    Diet,
    Nutrients,
    Formulations,
    FormulationNutrients
  RawMaterial* = object
    code*: string
    description*: string
    description2*: string
    cost*: string
    round*: int
    group*: string
    quantity*: string
    nutrients*: seq[Nutrient]
  Nutrient* = object
    code*: string
    name*: string
    quantity*: string
    group*: string
    unit*: UnitCode
    abbrev*: string
    description*: string
    min*: string
    max*: string
  Formulation* = object
    code*: string
    name*: string
    stock*: string
    feed*: string
    raw_materials*: seq[RawMaterial]
    datestamp*: string
    optml*: string
    cost*: string
  NutrientsVersion* = Table[int,seq[Nutrient]]
  FormNutVersions* = Table[string,NutrientsVersion]
  UnitCode* = enum
    None = "",
    Percent = "%",
    mg = "mg",
    g = "g"

proc toFileType*(s: string): FileType = 
  case s
  of "RM": return RawMat
  of "NUT": return Nutrients
  of "FM": return Formulations
  of "FMNUT": return FormulationNutrients

proc toUnitCode*(s: string): UnitCode =
  case s
  of "%": return Percent
  of "mg": return mg
  of "g": return g
  else: return None

proc NametoUnitCode*(s: string): UnitCode =
  if s[^1] == '%':
    return Percent
  elif (s.len > 2 and s[^2..^1] == "mg") or (s.len > 4 and s[^4..^1] == "(mg)"):
    return mg
  elif s.len > 3 and s[^3..^1] == "(g)" or s[^1] == 'g':
    return g
  return None

let sym_re = re"""[\s\(\)\/\\]"""
proc nameToCode*(n: Nutrient): string = 
  var c = n.name
  c = c.replace("%","pcnt")
  c = c.replace(sym_re,"")
  return c

proc RMtoNutrient*(s: string): Nutrient =
  try:
    let m = s.split(",")
    if m.len == 2:
      var nutrient: Nutrient
      nutrient.quantity = m[0]
      nutrient.name = m[1]
      nutrient.code = nutrient.nameToCode
      if nutrient.quantity == "":
        nutrient.quantity = "0"
      if nutrient.quantity[0] == '.':
        nutrient.quantity = "0" & nutrient.quantity
      return nutrient
  except:
    let _ = "Error splitting by comma"
  return Nutrient()

proc FormulaToRawMaterial*(s: string): RawMaterial =
  try:
    let m = s.split(",")
    if m.len == 2:
      var rm: RawMaterial
      rm.quantity = m[1]
      rm.code = m[0]
      if rm.quantity == "":
        rm.quantity = "0"
      if rm.quantity[0] == '.':
        rm.quantity = "0" & rm.quantity
      return rm
  except:
    let _ = "Error splitting by comma"
  return RawMaterial()

proc isRawMaterial*(rm: RawMaterial): bool =
  return rm.description != ""

proc isNutrient*(m: Nutrient): bool =
  if m.name != "": return true
  return false

proc isNumeric*(s: string): bool =
  for char in s:
    if not char.isDigit() or char != '.':
      return false
  return true

